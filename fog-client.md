# Instaling the FOG client
- Go to the FOG server's web interface on the target computer
- Click "FOG Client" at the bottom of the page
- Download the "Smart Installer" and run it
	- Enter the address of the server on its internal network when asked for server address