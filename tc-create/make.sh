#!/bin/sh

rm -rf corepure64.gz isolinux remastered-corepure64.gz unpacked vmlinuz64 newiso TC-remastered.iso

mkdir /tmp/core64
mount -t iso9660 -o loop CorePure64-11.1.iso /tmp/core64
cp -r -a -p /tmp/core64/boot/* .
umount /tmp/core64
rmdir /tmp/core64

mkdir unpacked
cd unpacked
zcat ../corepure64.gz | cpio -i -H newc -d >/dev/null

cp ../bootlocal.sh opt/
cp ../shadow etc/

mkdir -p opt/tce/optional
cd opt/tce/optional
URL=
TCZS=`cat ../../../../extratczs.txt`
for tcz in $TCZS
  do
    wget http://repo.tinycorelinux.net/11.x/x86_64/tcz/$tcz
done

cd ../../..
sh -c "find . | cpio -o -H newc | gzip -2" > ../remastered-corepure64.gz

cd ..
advdef -z4 remastered-corepure64.gz

sed -i -e 's/timeout 300/timeout 1/' isolinux/isolinux.cfg

mkdir -p newiso/boot
mv remastered-corepure64.gz newiso/boot/corepure64.gz
mv vmlinuz64 newiso/boot/
mv isolinux newiso/boot/
mkisofs -l -J -R -V TC-custom -no-emul-boot -boot-load-size 4  -boot-info-table -b boot/isolinux/isolinux.bin  -c boot/isolinux/boot.cat -o TC-remastered.iso newiso
isohybrid TC-remastered.iso

