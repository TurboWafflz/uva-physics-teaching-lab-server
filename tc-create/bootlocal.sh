#!/bin/sh
# put other system startup commands here
su tc -c "find /opt/tce/optional -name '*.tcz' -exec tce-load -i {} \;"
modprobe dm-mod
# Need this because the bnx2 firmware tcz isn't installed until
# after the bnx2 kernel module has already tried and failed.
if (lsmod|grep -q bnx2 >/dev/null 2>&1)
  then
    rmmod bnx2
    modprobe bnx2
fi
udhcpc eth0
sleep 10

cp /usr/local/etc/ssh/ssh_config.orig /usr/local/etc/ssh/ssh_config
cp /usr/local/etc/ssh/sshd_config.orig /usr/local/etc/ssh/sshd_config
/usr/local/etc/init.d/openssh start &

if [ "$MAILHUB" = "" ]
  then
    export MAILHUB=smtp.mail.virginia.edu
fi

HWADDR=`ifconfig eth0 | grep HWaddr | awk '{print $NF}'`
IPADDR=`ifconfig eth0 | grep 'inet addr' | awk '{print $2}' | sed -e 's/addr://'`
SERNO=`dmidecode | grep 'Serial Number:' | head -1 | awk '{print $NF}'`

cat - <<EOF | while read L; do sleep "1"; echo "$L"; done | nc $MAILHUB 25
HELO tinycore.phys.virginia.edu
MAIL From: me@example.com
RCPT To: me@example.com
DATA
From: tc@$IPADDR
To: me@example.com
Subject: TinyCore booted on $IPADDR ($HWADDR/$SERNO)

TinyCore has been booted on $IPADDR ($HWADDR).

DATE: `date`
SERNO: $SERNO
IPADDR: $IPADDR
HWADDR: $HWADDR
.
QUIT
EOF
