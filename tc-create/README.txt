* Generate a password hash for the tc account, like this:

  openssl passwd -1

  Copy the password into the "shadow" file in the top level of this tree,
  as the hash for the "tc" account, in place of INSERTPASSWORDHASHHERE.

* Add the names of any extra tcz packages to extratczs.txt.

* Modify bootlocal.sh to your taste. (In particular, change the e-mail address "me@example.com" to which notifications will be sent.)

* type "sh make.sh".  An ISO file named TC-remastered will be created.
