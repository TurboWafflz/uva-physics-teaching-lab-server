# Managing Devices

## Enrolling and Deploying Devices
- Make sure the device is on the FOG server's network
- Disable secure boot
- Enable legacy booting
- Configure device to PXE boot by default
- Boot device
- Select the full enroll option and follow onscreen prompts, making sure to specify that you would like to deploy an image

## Capturing an image from a device
- Select the device from the "Hosts" tab of the FOG web interface
- Click the "Basic Tasks" tab
- Click "Capture" and select the options you want

# Devices
## General, Nuclear, Optical
- Plain Windows 10 image

## Sound
- Windows 10 with LabVIEW

## Signal
- Windows 10 with MATLAB and LabVIEW