# Server Cloning

- Boot live USB on target (Instructions and files for creating one based on TinyCore are in the `tc-create` directory)
- Register target MAC address at https://netreg.virginia.edu
- Restart udhcpc on target
- Partition target's internal disk (assumed to be /dev/sda, change if necessary)
	- sda1 1G Type 8300 Starting at 2048
	- sda2 Remainder of disk Type 8e00
	- sda3 1G Type ef02 Starting at beginning
- Setup LVM
	- `pvcreate /dev/sda`
	- `vgcreate almalinux /dev/sda`
	- `lvcreate -L 1024M -n swap almalinux`
	- `lvcreate -L100%FREE -n root almalinux`
- Create filesystems
	- `mkfs.xfs -L /boot /dev/sda1`
	- `mkfs.xfs -L / /dev/almalinux/root`
	- `mkswap /dev/almalinux/swap`
- Mount root and boot partitions
	- `mkdir /mnt/tmp`
	- `mount /dev/sda1 /mnt/tmp/boot`
	- `mount /dev/almalinux/root /mnt/tmp`
- Copy files
	- `rsync -av -x -X <address of source>:/boot/ /mnt/tmp/boot/`
	- `rsync -av -X -X <address of source>:/boot/ /mnt/tmp/`