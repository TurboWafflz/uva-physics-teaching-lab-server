# Resetting the password on a Windows computer
- Boot a live Linux drive
- Install chntpw
- Mount Windows disk
- Go to `<windows mount path>/Windows/System32/config`
- Run `chntpw -u <username> SAM`
- Select the option you want